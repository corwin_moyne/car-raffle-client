import { Component, Input, OnInit } from '@angular/core';
import { User } from 'src/app/shared/models/user.model';

@Component({
  selector: 'app-username-password',
  templateUrl: './username-password.component.html',
  styleUrls: ['./username-password.component.scss']
})
export class UsernamePasswordComponent implements OnInit {

  @Input() user: User;

  constructor() { }

  ngOnInit() {
  }

}
