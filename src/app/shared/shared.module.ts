import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ClipboardModule } from 'ngx-clipboard';

import { UsernamePasswordComponent } from './components';
import { AuthService } from './services/auth.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    HttpClientModule,
  ],
  declarations: [UsernamePasswordComponent],
  exports: [
    // angular
    CommonModule,
    FormsModule,
    RouterModule,
    HttpClientModule,

    // 3rd party
    ClipboardModule,

    // components
    UsernamePasswordComponent
  ],
  providers: [AuthService]
})
export class SharedModule { }
