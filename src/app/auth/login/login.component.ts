import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/shared/models/user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  user: User;

  constructor(private router: Router) { }

  ngOnInit() {
    this.user = new User();
  }

  onSubmit(): void {
    this.router.navigateByUrl('/home');
  }
}
