import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { SharedModule } from '../shared/shared.module';
import { RaffleLinkComponent } from './raffle-link/raffle-link.component';
import { RaffleComponent } from './raffle.component';
import { RaffleRoutingModule } from './raffle.module.routing';
import { RaffleEffects } from './state/raffle.effects';
import { reducer } from './state/raffle.reducer';

@NgModule({
  imports: [
    SharedModule,
    RaffleRoutingModule,
    StoreModule.forFeature('raffleState', reducer),
    EffectsModule.forFeature([RaffleEffects])
  ],
  declarations: [RaffleComponent, RaffleLinkComponent]
})
export class RaffleModule { }
