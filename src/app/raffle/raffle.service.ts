import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RaffleService {

  constructor(private httpClient: HttpClient) { }

  getRaffle(raffleId: string): Observable<any> {
    return this.httpClient.get('../json/raffle.json');
  }

}
