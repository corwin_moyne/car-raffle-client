export interface Raffle {
    areAllNumbersSold: boolean;
    costPerNumber: number;
    created: Date;
    currencyCode: string;
    images: Array<string>;
    locked: boolean;
    lottery: any;
    number: LotteryNumber[];
    questionId: string;
    title: string;
    totalNumbersRemaining: number;
    totalNumbersSold: number;
    userId: string;
}

export interface LotteryNumber {
    _id: string;
    number: number;
    sold: boolean;
}