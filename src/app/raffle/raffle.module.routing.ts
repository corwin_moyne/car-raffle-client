import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RaffleComponent } from './raffle.component';

const routes: Routes = [
    { path: ':id', component: RaffleComponent }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
    declarations: [],
})
export class RaffleRoutingModule { }