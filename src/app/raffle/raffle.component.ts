import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { takeWhile } from 'rxjs/operators';

import { Raffle } from './raffle';
import * as raffleActions from './state/raffle.actions';
import * as fromRaffle from './state/raffle.reducer';

@Component({
  selector: 'app-raffle',
  templateUrl: './raffle.component.html',
  styleUrls: ['./raffle.component.scss']
})
export class RaffleComponent implements OnInit, OnDestroy {

  raffle: Raffle;
  selectedNumbers: any[];
  componentActive = true;

  constructor(
    private store: Store<fromRaffle.State>,
    private router: Router) { }

  ngOnInit() {
    this.store.dispatch(new raffleActions.Load('12345'));
    this.store.pipe(select(fromRaffle.getRaffle),
      takeWhile(() => this.componentActive))
      .subscribe(raffle => {
        this.raffle = raffle;
        this.raffle.created = new Date();
      });

    this.store.pipe(select(fromRaffle.getSelectedNumbers),
      takeWhile(() => this.componentActive))
      .subscribe(selectedNumbers => {
        this.selectedNumbers = selectedNumbers;
        console.log(this.selectedNumbers);
      });
  }

  ngOnDestroy(): void {
    this.componentActive = false;
  }

  raffleLink(): string {
    return window.location.href;
  }

  toggleBuy(number): void {
    if (!this.isNumberSelected(number)) {
      this.store.dispatch(new raffleActions.AddSelectedNumber(number));
    } else {
      this.store.dispatch(new raffleActions.RemoveSelectedNumber(number));
    }
  }

  isNumberSelected(number): boolean {
    return this.selectedNumbers.includes(number);
  }

  hasSelections(): boolean {
    return this.selectedNumbers.length > 0;
  }

  clearSelections(): void {
    this.store.dispatch(new raffleActions.ClearSelectedNumbers([]));
  }

  onSubmit(): void {
    // this.cartService.updateCart(this.selectedNumbers);
    this.router.navigateByUrl('checkout');
  }

}
