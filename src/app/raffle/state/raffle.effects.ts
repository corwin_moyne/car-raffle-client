import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';

import { RaffleService } from '../raffle.service';
import * as raffleActions from './raffle.actions';

@Injectable()
export class RaffleEffects {

    constructor(
        private actions$: Actions,
        private raffleService: RaffleService) { }

    @Effect()
    loadRaffle$ = this.actions$.pipe(
        ofType(raffleActions.RaffleActionTypes.Load),
        map((action: raffleActions.Load) => action.payload),
        mergeMap((raffleId: string) => this.raffleService.getRaffle(raffleId)
            .pipe(
                map(raffle => new raffleActions.LoadSuccess(raffle)),
                catchError(error => of(new raffleActions.LoadFail(error))
                ))
        ));
}