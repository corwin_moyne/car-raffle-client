import { Action } from '@ngrx/store';

import { LotteryNumber, Raffle } from '../raffle';

export enum RaffleActionTypes {
    GetRaffle = '[Raffle] Get Raffle',
    GetSelectedNumbers = '[Raffle] Get Selected Numbers',
    AddSelectedNumber = '[Raffle] Add Selected Numbers',
    RemoveSelectedNumber = '[Raffle] Remove Selected Number',
    ClearSelectedNumbers = '[Raffle] Clear Selected Numbers',
    InitializeRaffle = '[Raffle] Initialize Raffle',
    Load = '[Raffle] Load Raffle',
    LoadSuccess = '[Raffle] Load Success',
    LoadFail = '[Raffle] Load Fail'
}

export class GetRaffle implements Action {
    readonly type = RaffleActionTypes.GetRaffle;

    constructor(public payload: Raffle) { }
}

export class GetSelectedNumbers implements Action {
    readonly type = RaffleActionTypes.GetSelectedNumbers;

    constructor(public payload: LotteryNumber[]) { }
}

export class AddSelectedNumber implements Action {
    readonly type = RaffleActionTypes.AddSelectedNumber;

    constructor(public payload: LotteryNumber) { }
}

export class RemoveSelectedNumber implements Action {
    readonly type = RaffleActionTypes.RemoveSelectedNumber;

    constructor(public payload: LotteryNumber) { }
}

export class ClearSelectedNumbers implements Action {
    readonly type = RaffleActionTypes.ClearSelectedNumbers;

    constructor(public payload: LotteryNumber[]) { }
}

export class InitializeRaffle implements Action {
    readonly type = RaffleActionTypes.InitializeRaffle;

    constructor(public payload: Raffle) { }
}

export class Load implements Action {
    readonly type = RaffleActionTypes.Load;

    constructor(public payload: string) { }
}

export class LoadSuccess implements Action {
    readonly type = RaffleActionTypes.LoadSuccess;

    constructor(public payload: Raffle) { }
}

export class LoadFail implements Action {
    readonly type = RaffleActionTypes.Load;

    constructor(public payload: string) { }
}

export type RaffleActions = GetRaffle
    | GetSelectedNumbers
    | AddSelectedNumber
    | RemoveSelectedNumber
    | ClearSelectedNumbers
    | InitializeRaffle
    | Load
    | LoadSuccess
    | LoadFail;