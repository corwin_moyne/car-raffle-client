import { createFeatureSelector, createSelector } from '@ngrx/store';

import * as fromRoot from '../../state/app.state';
import { Raffle } from '../raffle';
import { RaffleActions, RaffleActionTypes } from './raffle.actions';

export interface State extends fromRoot.State {
    raffleState: RaffleState;
}

export interface RaffleState {
    raffle: Raffle;
    selectedNumbers: any[];
}

const initialState: RaffleState = {
    raffle: null,
    selectedNumbers: []
};

const getRaffleFeatureState = createFeatureSelector<RaffleState>('raffleState');

// selectors
export const getRaffle = createSelector(
    getRaffleFeatureState,
    state => state.raffle
);

export const getSelectedNumbers = createSelector(
    getRaffleFeatureState,
    state => state.selectedNumbers
);

// reducer
export function reducer(state = initialState, action: RaffleActions): RaffleState {
    switch (action.type) {
        case RaffleActionTypes.AddSelectedNumber:
            return {
                ...state,
                selectedNumbers: [...state.selectedNumbers, action.payload]
            };
        case RaffleActionTypes.RemoveSelectedNumber:
            return {
                ...state,
                selectedNumbers: [...state.selectedNumbers.filter(number => number !== action.payload)]
            };
        case RaffleActionTypes.ClearSelectedNumbers:
            return {
                ...state,
                selectedNumbers: action.payload
            };
        case RaffleActionTypes.LoadSuccess:
        return {
            ...state,
            raffle: action.payload
        };
        default:
            return state;
    }
}