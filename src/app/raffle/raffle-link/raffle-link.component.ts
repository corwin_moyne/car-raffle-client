import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-raffle-link',
  templateUrl: './raffle-link.component.html',
  styleUrls: ['./raffle-link.component.scss']
})
export class RaffleLinkComponent {

  @Input() raffleLink: string;


}
